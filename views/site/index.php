<?php

/* @var $this yii\web\View */

use cakebake\bootstrap\select\BootstrapSelectAsset;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Fruit;

// Заголовок страницы
$this->title = 'analytics.loc';

// Подключим выпадающие списки
BootstrapSelectAsset::register($this, [
	'selector' => '.detailing',
	'selectpickerOptions' => [
		'size' => 3,
		'width' => '210px',
	],
]);

// Поддключим скрипт для отрисоки графиков
HighchartsAsset::register($this)->withScripts();

$js = <<<JS

loadingAjaxFlag = false;

// Отправим форму на аякс
$(document).on('beforeSubmit', '#analytics-form', function () {

    /* Отправим запрос к серверу */
    if (!loadingAjaxFlag) {
        loadingAjaxFlag = true;

        $("#analytics-form-content")
            .removeClass('analytics-form-content-active')
            .html('<div class="analytics-form-loader"></div>');
        $("#analytics-form-button").attr('disabled', 'disabled');
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action') + '?send=true',
            data: $(this).serialize(),
            success: function (data) {
                $('#analytics-form-content').addClass('analytics-form-content-active');
                Highcharts.chart('analytics-form-content', data);
                loadingAjaxFlag = false;
                $("#analytics-form-button").removeAttr("disabled")
            }
        });
    }

    return false;
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

<div class="site-analytics">
    <h1>Статистика</h1>
    <p>Выберите интересующий фрукт и год за который нужно получить статистику</p>

	<?php $form = ActiveForm::begin([
		'id'          => 'analytics-form',
		'layout'      => 'horizontal',
		'fieldConfig' => [
			'template'     => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-1 control-label'],
		],
	]); ?>

	<?= $form->field($model, 'fruit_id')
		->dropdownList(Fruit::getAll(), [
			'class' => 'detailing'
		]) ?>

	<?= $form->field($model, 'year', [
		'enableAjaxValidation' => true,
		'template' => "{label}\n<div class=\"col-lg-1\">{input}</div>\n<div class=\"col-lg-10\">{error}</div>",
	])
		->textInput([
			'size'      => 4,
			'maxlength' => 4,
		]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
			<?= Html::submitButton('Получить данные', [
				'id'  => 'analytics-form-button',
				'class' => 'btn btn-primary',
            ]) ?>
        </div>
    </div>

	<?php ActiveForm::end(); ?>

    <div id="analytics-form-content">
        <div class="analytics-form-bg"></div>
    </div>

</div>
