<?php

use yii\db\Migration;

class m170926_195749_create_stats extends Migration
{
	public function up()
	{
		$this->createTable('stats', [
			'id'         => $this->primaryKey(),
			'fruit_id'   => $this->integer(),
			'timestamp'  => $this->timestamp(),
		]);
	}

	public function down()
	{
		$this->dropTable('stats');
	}
}
