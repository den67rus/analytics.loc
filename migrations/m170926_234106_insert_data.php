<?php

use yii\db\Migration;

class m170926_234106_insert_data extends Migration
{
    public function up()
    {
    	// Добавим имена
	    $this->insert('fruit', ['name' => 'Яблоки']);
	    $this->insert('fruit', ['name' => 'Груши']);
	    $this->insert('fruit', ['name' => 'Апельсины']);

	    // Сгенерируем статистику
	    $this->genStats();
    }

    public function down()
    {
    	// Очистим таблицы
        $this->truncateTable('fruit');
        $this->truncateTable('stats');
    }

	/**
	 * Генерируем рандомную статистику
	 */
    public function genStats() {

	    // id фруктов для генерации
	    $fruit = [1,2,3];

	    // Пройдемся по годам
	    for ($year = 2010; $year < 2017; $year++) {

		    // Пройдемся по месяцам
		    for ($month = 1; $month < 13; $month++) {

			    // Пройдемся по дням месяца
			    for ($day = 1; $day < 28; $day++) {

				    // Пройдемся по всем фруктам
				    foreach ($fruit as $fruit_id) {

					    // Яблоки продавались только с 2013 года
					    if($fruit_id === 1 AND $year < 2013) {
						    continue;
					    }

					    // генерируем рандомное количество продаж в день
					    for ($rand = mt_rand(0, 10); $rand > 0; $rand--) {
						    $this->insert('stats', [
							    'fruit_id' => $fruit_id,
							    'timestamp' => $year . '-' . $month . '-' . $day . ' '
								    . mt_rand(0, 23) . ':' . mt_rand(0, 59) . ':' . mt_rand(0, 59),
						    ]);
					    }
				    }
			    }
		    }
	    }
    }
}
