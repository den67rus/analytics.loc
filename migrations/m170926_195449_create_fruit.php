<?php

use yii\db\Migration;

class m170926_195449_create_fruit extends Migration
{
	public function up()
	{
		$this->createTable('fruit', [
			'id'         => $this->primaryKey(),
			'name'       => $this->string(30),
		]);
	}

	public function down()
	{
		$this->dropTable('fruit');
	}
}
