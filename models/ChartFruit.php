<?php

namespace app\models;

use Yii;
use yii\base\Object;

/**
 * Генерирует объект для highcharts графиков
 * Class ChartFruit
 * @package app\models
 */
class ChartFruit extends Object
{
	/**
	 * @var array объект с данными для построения графика
	 */
	protected $data = [];

	/**
	 * @var int id фрукта
	 */
	protected $fruit_id;

	/**
	 * @var int год за который нжно получить статистику
	 */
	protected $year;

	/**
	 * ChartFruit constructor.
	 *
	 * @param int $fruit_id
	 * @param int $year
	 */
	public function __construct($fruit_id, $year)
	{
		$this->fruit_id = (int) $fruit_id;
		$this->year     = (int) $year;

		parent::__construct();
	}

	/**
	 * Возвращает массив для построения графика
	 * @return array
	 */
	public function getData()
	{
		// Получаем статистику
		$stats = Stats::getFruitYear($this->fruit_id, $this->year);
		$name = Fruit::getName($this->fruit_id);

		// Формируем серию данных
		$series = (new AnalyticsChartSeries())
			->setName($name)
			->setData($stats)
			->getData();

		// Создаем график
		return (new AnalyticsChart())
			->setChartTitle('Статистика по продажам фруктов «' . $name .'» с Января ' . (int) $this->year . ' по Декабрь ' . (int) $this->year)
			->setYAxisTitle('Количество продаж')
			->setXAxisType('category')
			->setXAxisTitle('Месяц')
			->loadSeries($series)
			->getData();
	}
}
