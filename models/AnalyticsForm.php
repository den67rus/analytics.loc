<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Модель для работы формы статистики
 * Class AnalyticsForm
 * @package app\models
 */
class AnalyticsForm extends Model
{
	/**
	 * @var int id фрукта в базе данных
	 */
    public $fruit_id;

	/**
	 * @var int год за который необходимо вернуть статистику
	 */
    public $year;

	/**
	 * @return array правила для обработки формы
	 */
    public function rules()
    {
        return [
            // Поля должны быть заполнены
            [['fruit_id', 'year'], 'required'],

            // Поля должны иметь цифровые значения
            [['fruit_id', 'year'], 'integer'],

	        // Проверяем, длину значения года
	        ['year', 'string', 'length' => [4, 4]],

	        // Проверяем значение года на адекватность
	        ['year', 'compare', 'compareValue' => 2000, 'operator' => '>=', 'type' => 'number'],
	        ['year', 'compare', 'compareValue' => 2099, 'operator' => '<=', 'type' => 'number'],

	        // Выполняем проверку возможности выборки из базе данных
	        ['year', 'validateYear'],
        ];
    }

	/**
	 * Проверяем наличие данных по переданному году и фрукту в базе данных
	 */
	public function validateYear()
	{
		if(!Stats::checkFruitYear($this->fruit_id, $this->year)) {
			$this->addError('year', 'Информация о фруктах «' . Fruit::getName($this->fruit_id) . '» '
			. 'за ' . (int) $this->year . ' год отсутствует в базе данных.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'fruit_id' => 'Фрукт',
			'year' => 'Год',
		];
	}
}
