<?php

namespace app\models;

use Yii;
use yii\base\Object;

/**
 * Class AnalyticsChart
 * @package app\models
 */
class AnalyticsChart extends Object
{
	/**
	 * @var array объект для построения графика
	 */
	protected $data = [];

	/**
	 * AnalyticsChart constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->setChartType('bar');
	}

	/**
	 * Устанавливаем тип графика
	 * @param string $type
	 * @return $this
	 */
	public function setChartType($type)
	{
		$this->data['chart']['type'] = $type;
		return $this;
	}

	/**
	 * Устанавливаем заголовок графика
	 * @param string $title
	 * @return $this
	 */
	public function setChartTitle($title)
	{
		$this->data['title']['text'] = $title;
		return $this;
	}

	/**
	 * Устанавливаем подзаголовок графика
	 * @param string $subTitle
	 * @return $this
	 */
	public function setChartSubTitle($subTitle)
	{
		$this->data['subtitle']['text'] = $subTitle;
		return $this;
	}

	/**
	 * Устанавливаем тип оси Х
	 * @param string $type
	 * @return $this
	 */
	public function setXAxisType($type)
	{
		$this->data['xAxis']['type'] = $type;
		return $this;
	}

	/**
	 * Устанавливаем тип оси Y
	 * @param string $type
	 * @return $this
	 */
	public function setYAxisType($type)
	{
		$this->data['yAxis']['type'] = $type;
		return $this;
	}

	/**
	 * Устанавливаем заголовок оси Х
	 * @param string $title
	 * @return $this
	 */
	public function setXAxisTitle($title)
	{
		$this->data['xAxis']['title']['text'] = $title;
		return $this;
	}

	/**
	 * Устанавливаем заголовок оси Y
	 * @param string $title
	 * @return $this
	 */
	public function setYAxisTitle($title)
	{
		$this->data['yAxis']['title']['text'] = $title;
		return $this;
	}

	/**
	 * Загружаем данные
	 * @param $data
	 * @return $this
	 */
	public function loadSeries($data)
	{
		$this->data['series'][] = $data;
		return $this;
	}

	/**
	 * Возвращает массив для построения графика
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
}
