<?php

namespace app\models;

use Yii;
use yii\base\Object;

/**
 * Class AnalyticsChartSeries
 * @package app\models
 */
class AnalyticsChartSeries extends Object
{
	/**
	 * @var array объект с данными для построения графика
	 */
	protected $data = [];

	/**
	 * Устанавливаем название набора
	 * @param string $title
	 * @return $this
	 */
	public function setName($title)
	{
		$this->data['name'] = $title;
		return $this;
	}

	/**
	 * Добавим данные для графика
	 * @param array $array массив с [номер месяца => количество]
	 * @return $this
	 */
	public function setData($array)
	{
		$items = [];
		foreach ($this->getMonths() as $num => $name) {
			$items[] = [
				'name' => $name,
				'y' => isset($array[$num]) ? (int) $array[$num] : 0,
			];
		}
		$this->data['data'] = $items;
		return $this;
	}

	/**
	 * Возвращает массив для построения графика
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Возвращает месяцы для построения графика
	 * @return array
	 */
	protected function getMonths()
	{
		return [
			'1'  => 'Январь',
			'2'  => 'Февраль',
			'3'  => 'Март',
			'4'  => 'Апрель',
			'5'  => 'Май',
			'6'  => 'Июнь',
			'7'  => 'Июль',
			'8'  => 'Август',
			'9'  => 'Сентябрь',
			'10' => 'Октябрь',
			'11' => 'Ноябрь',
			'12' => 'Декабрь',
		];
	}
}
