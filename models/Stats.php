<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stats".
 *
 * @property integer $id
 * @property integer $fruit_id
 * @property string $timestamp
 */
class Stats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fruit_id'], 'integer'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fruit_id' => 'Fruit ID',
            'timestamp' => 'Timestamp',
        ];
    }

	/**
	 * Определяет наличие данных по фрукту и году в базе данных
	 * @param int $fruit_id id фрукта
	 * @param int $year год
	 *
	 * @return bool
	 */
    public static function checkFruitYear($fruit_id, $year)
    {
    	$db = self::find()
		    ->where([
		    	'and',
			    ['=', 'fruit_id', (int) $fruit_id],
			    ['like', 'timestamp', (int) $year . '-'],
		    ])
		    ->one();

    	return !empty($db);
    }

	/**
	 * Возвращает массив со статистикой о продаже фруктов по месяцам
	 * @param int $fruit_id id фрукта
	 * @param int $year год
	 * @return array
	 */
    public static function getFruitYear($fruit_id, $year)
    {
	    $result = [];
	    foreach (self::getMonths() as $key => $value) {
		    $count = self::find()
			    ->where([
				    'and',
				    ['=', 'fruit_id', (int) $fruit_id],
				    ['like', 'timestamp', (int) $year . '-' . $value],
			    ])
			    ->count();
		    $result[$key] = $count;
	    }
	    return $result;
    }

	/**
	 * Возвращает месяцы для запросов к базе данных
	 * @return array
	 */
	protected static function getMonths()
	{
		return [
			'1'  => '01',
			'2'  => '02',
			'3'  => '03',
			'4'  => '04',
			'5'  => '05',
			'6'  => '06',
			'7'  => '07',
			'8'  => '08',
			'9'  => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		];
	}
}
