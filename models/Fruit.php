<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fruit".
 *
 * @property integer $id
 * @property string $name
 */
class Fruit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fruit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

	/**
	 * Получает все фрукты из базы
	 * @return array
	 */
    public static function getAll()
    {
    	$items = self::find()
		    ->asArray()
		    ->all();

    	$array = [];
    	foreach ($items as $item) {
		    $array[$item['id']] = $item['name'];
	    }

	    return $array;
    }

	/**
	 * Возвращает название фрукта по id
	 * @param int $id
	 *
	 * @return string
	 */
    public static function getName($id)
    {
	    $name = self::findOne((int) $id);
	    return empty($name)?: $name->name;
    }
}
