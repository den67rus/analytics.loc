<?php

namespace app\controllers;

use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\Response;
use app\models\AnalyticsForm;
use app\models\ChartFruit;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

	/**
	 * @param bool $send
	 *
	 * @return array|string
	 */
    public function actionIndex($send = false)
    {
	    $model = new AnalyticsForm();
	    if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
		    Yii::$app->response->format = Response::FORMAT_JSON;

		    // Если запрос на валидацию
		    if ((bool)$send === false) {
			    return ActiveForm::validate($model);
		    }

		    // Если запрос на отдачу контента
		    if ($model->validate()) {
			    return (new ChartFruit($model->fruit_id, $model->year))->getData();
		    } else {
			    return ActiveForm::validate($model);
		    }
	    }

	    return $this->render('index', [
		    'model' => $model,
	    ]);
    }
}
