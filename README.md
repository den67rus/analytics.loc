Установка
============================

1. Установим asset плагин
    ```
    composer global require "fxp/composer-asset-plugin:^1.3.1"
    ```
2. Установим сам скрипт
    ```
    composer create-project den67rus/analytics.loc
    ```

3. Создать базу данных `analytics.loc` или указать свою в `config/db.php`

4. Применить миграции базы данных

   ```
   yii migrate
   ```